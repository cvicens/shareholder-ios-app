//
//  Top50TableViewController.swift
//  shareholder-ios-app
//
//  Created by Carlos Vicens on 06/06/2018.
//  Copyright © 2018 Carlos Vicens. All rights reserved.
//

import UIKit
import FeedHenry

class Top50TableViewController: UITableViewController {
    var subject : Subject?
    
    var shareholders = [Shareholder]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        print("subject = \(String(describing: self.subject))")
        
        if let subject = self.subject {
            self.title = "Hi \(subject.uid)"
            // Get TOP50 shareholders
            getTop50(regid: subject.orgId)
        } else {
            self.title = "Hi John Doe"
        }
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.shareholders.count
    }

    func getTop50(regid: String) {
        //let args = ["key1": "value1", "key2": "value2"] as [String : AnyObject]
        FH.cloud(path: "/top50/\(regid)",
                 method: HTTPMethod.GET,
                 //args: args,
                 completionHandler: {(resp: Response, error: NSError?) -> Void in
                    if error != nil {
                        print("Cloud Call Failed: " + (error?.localizedDescription)!)
                        return
                    }
                    if let parsedResponse = resp.parsedResponse as NSDictionary?,
                        let status = parsedResponse.object(forKey: "status") as? String,
                        let statusCode = parsedResponse.object(forKey: "statusCode") as? Int,
                        let top50 = parsedResponse.object(forKey: "top50") as! NSArray?  {
                        //print("Success: \(top50.count)")
                        self.shareholders = self.toArray(top50)
                        self.tableView.reloadData()
                    } else {
                        print("Error in parsedResponse! \(resp)")
                    }
        })
    }
    
    func toArray(_ top50: NSArray) -> [Shareholder] {
        var shareholders = [Shareholder]()
        for case let _item as NSDictionary in top50 {
            print(_item)
            if let investorId = _item.object(forKey: "investorId") as? Int64,
                let name = _item.object(forKey: "name") as? String,
                let accountType = _item.object(forKey: "accountType") as? String,
                let percentage = _item.object(forKey: "percentage") as? Float,
                let ranking = _item.object(forKey: "ranking") as? Int,
                let countryOfResidence = _item.object(forKey: "countryOfResidence") as? String,
                let citizenship = _item.object(forKey: "citizenship") as? String,
                let numberOfHoldings = _item.object(forKey: "noOfHoldings") as? Int {
                shareholders.append(Shareholder(investorId: investorId, name: name, accountType: accountType, percentage: percentage, ranking: ranking, countryOfResidence: countryOfResidence, citizenship: citizenship, numberOfHoldings: numberOfHoldings))
            } else {
                print("Error transforming from NSDictionaty to Shareholder")
            }
        }
        return shareholders
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "shareholderCellIdentifier"

        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? ShareholderCell  else {
            fatalError("The dequeued cell is not an instance of MealTableViewCell.")
        }

        // Fetches the appropriate meal for the data source layout.
        let shareholder = shareholders[indexPath.row]
        
        cell.nameLabel.text = shareholder.name
        cell.numberOfHoldingsLabel.text = "\(String(shareholder.numberOfHoldings)) holdings"
        cell.percentageLabel.text = "\(lroundf(shareholder.percentage))%"
        
        return cell
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
