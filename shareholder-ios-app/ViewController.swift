//
//  ViewController.swift
//  shareholder-ios-app
//
//  Created by Carlos Vicens on 06/06/2018.
//  Copyright © 2018 Carlos Vicens. All rights reserved.
//

import UIKit
import FeedHenry

class ViewController: UIViewController {
    var sdkInit : Bool = false
    
    var policyId : String = "generic"
    
    var subject : Subject?

    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var loginButton: UIButton!
    
    @IBOutlet weak var messageLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // FH.init using Swift FH sdk
        // trailing closure Swift syntax
        self.messageLabel.text = "Init in progress..."
        self.loginButton.isEnabled = false
        FH.init { (resp:Response, error: NSError?) -> Void in
            if let error = error {
                self.messageLabel.text = "FH init in error \(error.localizedDescription)"
                
                print("Error: \(error)")
                return
            }
            self.sdkInit = true
            self.loginButton.isEnabled = true
            self.messageLabel.text = ""
            print("Response: \(String(describing: resp.parsedResponse))")
        }
    }

    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onLogin(_ sender: Any) {
        self.login()
    }
    
    func login() {
        self.loginButton.isEnabled = false
        self.messageLabel.text = "Login in progres..."
        self.usernameTextField.isEnabled = false
        self.passwordTextField.isEnabled = false
        FH.auth(policyId: self.policyId, userName: self.usernameTextField.text!, password: self.passwordTextField.text!, completionHandler: { (resp:Response, error: NSError?) -> Void in
            self.loginButton.isEnabled = true
            self.usernameTextField.isEnabled = true
            self.passwordTextField.isEnabled = true
            self.messageLabel.text = ""
            if let error = error {
                self.messageLabel.text = "Authentication error \(error.localizedDescription)"
                print("Error: \(error)")
                return
            }
            print("Response: \(String(describing: resp.parsedResponse))")
            
            if let parsedResponse = resp.parsedResponse as NSDictionary?,
               let uid = parsedResponse.object(forKey: "uid") as? String,
               let orgId = parsedResponse.object(forKey: "orgId") as? String,
               let authorities = parsedResponse.object(forKey: "authorities") as? String,
               let sessionToken = parsedResponse.object(forKey: "sessionToken") as? String {
                let config : PushConfig = PushConfig()
                config.alias = uid
                config.categories = ["owner"]
                
                FH.pushRegister(deviceToken: nil, config: config, success: { (response) in
                    print("RHMAP push registration successful");
                }, error: {(response) in
                    print("RHMAP push registration error \(String(describing: response.parsedResponse))");
                    let alert = UIAlertController(title: "Alert", message: "Error while registering for Push Notifications", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default))
                    self.present(alert, animated: true, completion: nil)
                })
                
                self.subject = Subject(uid: uid, orgId: orgId, authorities: authorities, sessionToken: sessionToken)
                self.performSegue(withIdentifier: "top50", sender: nil)

            } else {
                let alert = UIAlertController(title: "Alert", message: "Response couldn't be parsed", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default))
                self.present(alert, animated: true, completion: nil)
                print("Response couldn't be parsed \(resp.rawResponseAsString ?? "No rawREsponseAsString!!!")")
                return
            }
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        // Create a new variable to store the instance of PlayerTableViewController
        let destinationVC = segue.destination as! Top50TableViewController
        destinationVC.subject = self.subject
    }

    func auth() {
        FH.auth(policyId: self.policyId, userName: self.usernameTextField.text!, password: self.passwordTextField.text!, completionHandler: { (response: Response, error: NSError?) -> Void in
            if let error = error {
                print("Error \(error)")
                return
            }
            if let response = response.parsedResponse as? [String: String]{
                if let status = response["status"], status == "ok" {
                    print("Response \(response)")
                } else if let status = response["status"], status == "error" {
                    let message = response["message"] ?? ""
                    print("OAuth failed \(message)")
                }
            }
        })
    }

}

