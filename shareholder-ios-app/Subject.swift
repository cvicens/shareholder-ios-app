//
//  Subject.swift
//  shareholder-ios-app
//
//  Created by Carlos Vicens on 07/06/2018.
//  Copyright © 2018 Carlos Vicens. All rights reserved.
//

import UIKit

struct Subject {
    var uid : String
    var orgId : String
    var authorities : String
    var sessionToken : String
    
    init(uid: String, orgId: String, authorities: String, sessionToken: String) {
        self.uid = uid
        self.orgId = orgId
        self.authorities = authorities
        self.sessionToken = sessionToken
    }
}
