//
//  AppDelegate.swift
//  shareholder-ios-app
//
//  Created by Carlos Vicens on 06/06/2018.
//  Copyright © 2018 Carlos Vicens. All rights reserved.
//

import UIKit
import FeedHenry

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        FH.pushEnabledForRemoteNotification(application: application)
        FH.sendMetricsWhenAppLaunched(launchOptions: launchOptions)
        
        return true
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        FH.pushRegister(deviceToken: deviceToken, success: { (response) in
            // Do something cool
            print("RHMAP push registration successful");
        }, error: {(response) in
            print("RHMAP push registration error \(String(describing: response.parsedResponse))");
        })
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("RHMAP push registration error \(error.localizedDescription)");
    }

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        self.pushNotificationHandler(application, didReceiveRemoteNotification:userInfo)
    }
    
    func pushNotificationHandler(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) -> Void {
        print(userInfo)
        if application.applicationState == .active {
            //write your code here when app is in foreground
            self.showAlert(title: "Info", message: "foreground: \(userInfo.description)")
        } else {
            //write your code here for other state
            self.showAlert(title: "Info", message: "other state: \(userInfo.description)")
        }

    }
    
    func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default))
        self.window?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

