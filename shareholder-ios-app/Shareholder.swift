//
//  Shareholder.swift
//  shareholder-ios-app
//
//  Created by Carlos Vicens on 07/06/2018.
//  Copyright © 2018 Carlos Vicens. All rights reserved.
//

import UIKit

struct Shareholder {
    var investorId : Int64
    var name : String
    var accountType : String
    var percentage: Float
    var ranking : Int
    var countryOfResidence : String
    var citizenship : String
    var numberOfHoldings : Int
    
    init(investorId : Int64,
        name : String,
        accountType : String,
        percentage: Float,
        ranking : Int,
        countryOfResidence : String,
        citizenship : String,
        numberOfHoldings : Int) {
        self.investorId = investorId
        self.name = name
        self.accountType = accountType
        self.percentage = percentage
        self.ranking = ranking
        self.countryOfResidence = countryOfResidence
        self.citizenship = citizenship
        self.numberOfHoldings = numberOfHoldings
    }
}
